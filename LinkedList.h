#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<string.h>
using namespace std;
class List{
	private:
		typedef struct node{
			string title, artist, album, genre;
			int key;
			node* next;
			node* previous;
		}* nodePtr;		
		nodePtr head;
		nodePtr tail;
		nodePtr temp;
		nodePtr curr;
		int count;		
	public:
		List(){
			head = NULL;
			tail = NULL;
			curr = NULL;
			temp = NULL;
		}
		void add_begin(string title_in, string artist_in, string album_in, string genre_in, int key_in);
		void add_end(string title_in, string artist_in, string album_in, string genre_in, int key_in);
		void delete_pos(int pos);
		void edit(string input, int pos);
		void print();
		void Top();
		void play(int pos);
		void sort();
		string recent(int pos);
		friend class Stack;
		friend class Queue;
		/*nodePtr find_node(int pos){
			curr = head;
			if (head==tail && head==NULL){
				return NULL;
			}
			for(int x = 1; x<pos; x++){
				if(x<pos && (curr->next==head || curr->next==NULL)){
					return NULL;
				}
				curr = curr->next;
			}
			return curr;
		};*/
};
class Stack{
	public:
		void top(List &stack);
		void push(List &stack, string title_in, string artist_in, string album_in, string genre_in, int key_in);
		void pop(List &stack);
};
class Queue{
	public:
		void enqueue(List &queue, string title_in, string artist_in, string album_in, string genre_in, int key_in);
		void dequeue(List &queue);
};

void List::add_begin(string title_in, string artist_in, string album_in, string genre_in, int key_in){
	nodePtr n = new node;
	n->title = title_in;
	n->artist = artist_in;
	n->album = album_in;
	n->genre = genre_in;
	n->key = key_in;
	n->next = head;
	n->previous = tail;
	curr = head;
	count++;
	if(head != NULL){
		head = n;
		curr->previous = head;
		tail->next = head; 
		return;
	}
	else{
		head = n;
		tail = n;
		head->next = n;
		tail->next = n;
		return;
	}
}
void List::add_end(string title_in, string artist_in, string album_in, string genre_in, int key_in){
	nodePtr n = new node;
	n->title = title_in;
	n->artist = artist_in;
	n->album = album_in;
	n->genre = genre_in;
	n->key = key_in;
	n->next = NULL;
	n->previous = NULL;
	count++;
	if(head != NULL){
		curr = head;
		while(curr->next != head && curr->next != NULL){
			temp = curr;
			curr = curr->next;
			curr->previous = temp;
		}
		curr->next = n;
		temp = curr;
		curr = curr->next;
		curr->previous = temp;
		tail = n;
		tail->next = head;
		head->previous = tail;
		return;
	}
	else{
		head = n;
		tail = n;
	}
	return;
}
void List::delete_pos(int pos){
	nodePtr delPtr = new node;
	curr = head;
	if (pos == 1){
		delPtr = curr;
		curr = curr->next;
		head = curr;
		tail->next = head;
		delete delPtr;
		cout << "Data Deleted" << endl;
		system("PAUSE");
		system("CLS");
		return;
	}
	if (head==tail && head==NULL){
		cout << "List is empty, nothing to delete" << endl;
		system("PAUSE");
		system("CLS");
		return;
	}
	for(int x = 1; x<pos; x++){
		if(x<pos && (curr->next==head || curr->next==NULL)){
			cout << "Position out of range" << endl;
			system("PAUSE");
			system("CLS");
			return;
		}
		temp = curr;
		curr = curr->next;
	}
	delPtr = curr;
	curr = curr->next;
	temp->next = curr;
	delete delPtr;
	cout << "Data Deleted" << endl;
	system("PAUSE");
	system("CLS");
	return;
}
void List::edit(string input, int pos){
	curr = head;
	if (pos == 1){
		curr->title = input;
		cout << "Data Updated" << endl;
		system("PAUSE");
		system("CLS");
		return;
	}
	if (head==tail && head==NULL){
		cout << "List is empty, nothing to edit" << endl;
		system("PAUSE");
		system("CLS");
		return;
	}
	for(int x = 1; x<pos; x++){
		if(x<pos && (curr->next==head || curr->next==NULL)){
			cout << "Position out of range" << endl;
			system("PAUSE");
			system("CLS");
			return;
		}
		curr = curr->next;
	}
	curr->title = input;
	cout << "Data Updated" << endl;
	system("PAUSE");
	system("CLS");
	return;
}
void List::print(){
	curr = head;
	int i = 1;
	if(head==tail && head==NULL){
		cout << "List is empty, nothing to display" << endl;
		return;
	}
	while(curr->next != head && curr->next!=NULL){
		cout << i << ". \tTitle: " << curr->title
			<< "\n\tArtist: " << curr->artist
			<< "\n\tAlbum: " << curr->album
			<< "\n\tGenre: " << curr->genre << endl;
		curr = curr->next;
		i++;
	}
	cout << i << ". \tTitle: " << curr->title
			<< "\n\tArtist: " << curr->artist
			<< "\n\tAlbum: " << curr->album
			<< "\n\tGenre: " << curr->genre << endl;
	return;
}
void List::Top(){
	curr = head;
	if(head==tail && head==NULL){
		cout << "List is empty, nothing to display" << endl;
		return;
	}
	cout << curr->title << endl;
	return;
}
void List::play(int pos){
	int loop = 0;
	curr = head;
	if (head==tail && head==NULL){
		cout << "List is empty, nothing to edit" << endl;
		system("PAUSE");
		system("CLS");
		return;
	}
	for(int x = 1; x<pos; x++){
		if(x<pos && (curr->next==head || curr->next==NULL)){
			cout << "Position out of range" << endl;
			system("PAUSE");
			system("CLS");
			return;
		}
		curr = curr->next;
	}
	while(loop<3){
		cout << "Now Playing: " << curr->title << " by " << curr->artist << endl;
		curr = curr->next;
		cout << "Next: " << curr->title << " by " << curr->artist << endl;
		curr = curr->previous;
		curr = curr->previous;
		cout << "Previous: " << curr->title << " by " << curr->artist << endl;
		curr = curr->next;
		cout << "\t======================" << endl
			 << "\t||       Menu       ||" << endl
			 << "\t======================" << endl
			 << "\t| (1) Play Previous  |" << endl
			 << "\t| (2) Play Next      |" << endl
			 << "\t| (3) Back           |" << endl
			 << "\t======================" << endl;
		cout << "Your response: ";
		cin >> loop;
		system("CLS");
		if(loop==1){
			curr = curr->previous;
		}
		if(loop==2){
			curr = curr->next;
		}
	}
	return;
}
void List::sort(){
	int x=1;
	for(int i = 1; i<=99; i++){
		curr=head;
		while(curr->next!=head){
			if(curr->key==i){
				cout << x << ". \tTitle: " << curr->title
					 << "\n\tArtist: " << curr->artist
					 << "\n\tAlbum: " << curr->album
					 << "\n\tGenre: " << curr->genre << endl;
				x++;
			}
			curr = curr->next;
		}
	}
}
string List::recent(int pos){
	string ret;
	curr = head;
	for(int x = 1; x<pos; x++){
		if(x<pos && (curr->next==head || curr->next==NULL)){
			cout << "Position out of range" << endl;
			system("PAUSE");
			system("CLS");
			return "n/a";
		}
		temp = curr;
		curr = curr->next;
	}
	ret = curr->title + " by " + curr->artist;
	return ret;
}
void Stack::top(List &stack){
	stack.Top();
}
void Stack::pop(List &stack){
	stack.delete_pos(1);
}
void Stack::push(List &stack, string title_in, string artist_in, string album_in, string genre_in, int key_in){
	stack.add_begin(title_in, artist_in, album_in, genre_in, key_in);
}
void Queue::dequeue(List &queue){
	queue.delete_pos(1);
}
void Queue::enqueue(List &queue, string title_in, string artist_in, string album_in, string genre_in, int key_in){
	queue.add_end(title_in, artist_in, album_in, genre_in, key_in);
}